﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Entidades.RegistroPase
{
    public class RegPaseRequest
    {
        public int CodPaseProduccion { get; set; }
        public int TipoPase { get; set; }
        public DateTime FechaPase { get; set; }
        public string Ticket { get; set; }
        public string Descripcion { get; set; }
        public string UbicacionFuente { get; set; }
        public int PaseInterno { get; set; }
        public int MotivoPase { get; set; }
        public int Compilacion { get; set; }
        public string CondicionesEspeciales { get; set; }
        public int LibreriaFuente { get; set; }
        public int LibreriaObjetoOtros { get; set; }
        public int LibreriaObjetoPFyLF { get; set; }
        public int Analista { get; set; }
        public string UsuarioRegistro { get; set; }
        public string Comentario { get; set; }
        public List<Objeto> Objetos { get; set; }
        public List<Archivo> Archivos { get; set; }
        public List<Sustento> Sustentos { get; set; }
    }

    public class Objeto
    {
        public int Tipo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }

    public class Archivo
    {
        public string Nombre { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public int Modulo { get; set; }
    }

    public class Sustento
    {
        public string Nombre { get; set; }
        public string Archivo { get; set; }
        public int CodLaserFiche { get; set; }
    }
}
