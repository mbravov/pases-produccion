﻿namespace Agrobanco.SGPP.Entidades
{
    public class ArchivoBE
    {
        public int CodArchivo { get; set; }
        public string Nombre { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public int Modulo { get; set; }
    }
}
