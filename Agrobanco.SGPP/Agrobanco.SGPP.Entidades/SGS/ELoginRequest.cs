﻿namespace Agrobanco.SGPP.Entidades.SGS
{
    public class ELoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string IpCliente { get; set; }
        public string UrlAcceso { get; set; }
        public string UrlPathName { get; set; }
        public string IdAplicacion { get; set; }
        public string token { get; set; }
    }
}
