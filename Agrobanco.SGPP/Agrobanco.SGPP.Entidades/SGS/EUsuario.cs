﻿using System;
using System.Collections.Generic;

namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EUsuario
    {

        public string Token { get; set; }


        public string UsuarioID { get; set; }
        public string NombreCompleto { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Cargo { get; set; }
        public string TipoUsuario { get; set; }
        public string Ticket { get; set; }
        public string CorreoElectronico { get; set; }
        public string UsuarioWeb { get; set; }
        public string UsuarioAD { get; set; }
        public string EstadoAD { get; set; }
        public string AgenciaPrincipal { get; set; }
        public string AgenciaPrincipalDescripcion { get; set; }
        public string OtrasAgencias { get; set; }
        public string CodigoFuncionario { get; set; }
        public string Vigencia { get; set; }
        public DateTime? VigenciaDesde { get; set; }
        public DateTime? VigenciaHasta { get; set; }
        public string EstadoBloqueo { get; set; }
        public string EstadoLogico { get; set; }
        public DateTime? FechaBloqueo { get; set; }
        public DateTime? FechaUltAcceso { get; set; }
        public int IntentosFallidos { get; set; }
        public DateTime? FechaUltimoCambioClave { get; set; }

        public List<EModulo> ListaOpcionesMenu { get; set; }
        public List<EAgencia> ListaAgencias { get; set; }

        public string UsuarioIdRegistro { get; set; }
        public string tipoOperacion { get; set; }
        public string tipoRegistro { get; set; }
        public string cambioEstadoAD { get; set; }

    }
}
