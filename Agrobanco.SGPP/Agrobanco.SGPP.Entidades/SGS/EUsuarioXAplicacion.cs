﻿namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EUsuarioXAplicacion
    {
        public string UsuarioWeb { get; set; }
        public int IdTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Apellidos { get; set; }
        public string Nombres { get; set; }
        public string Cargo { get; set; }
        public string Correo { get; set; }
        public string IdAgencia { get; set; }
        public string Agencia { get; set; }
        public int IdTipoUsuario { get; set; }
        public string TipoUsuario { get; set; }
        public int IdPerfil { get; set; }
        public string PerfilAbreviatura { get; set; }
        public string Perfil { get; set; }
        public int Estado { get; set; }
    }
}
