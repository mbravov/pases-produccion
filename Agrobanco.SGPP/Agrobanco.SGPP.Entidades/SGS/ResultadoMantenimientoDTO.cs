﻿using System.Collections.Generic;

namespace Agrobanco.SGPP.Entidades.SGS
{
    public class ResultadoMantenimientoDTO<T>
    {
        public ResultadoMantenimientoDTO()
        {
            eServicioResponse = new EServicioResponse();
            ResultadoBusquedaUsuario = new ResultadoBusquedaDTO<EUsuario>();
            ResultadoBusquedaAplicacion = new ResultadoBusquedaDTO<EAplicacion>();
            ResultadoBusquedaPerfil = new ResultadoBusquedaDTO<EAcceso>();
        }

        public T Model { get; set; }
        public List<EParametro> ListTipoDocumento { get; set; }
        public List<EParametro> ListTipoUsuario { get; set; }
        public List<EAgencia> ListAgencias { get; set; }

        public List<EPerfil> ListPerfil { get; set; }

        public List<EParametro> ListTipoIcono { get; set; }

        public List<EPermisosXPerfil> ListPermisosXPerfil { get; set; }

        public ResultadoBusquedaDTO<EUsuario> ResultadoBusquedaUsuario { get; set; }
        public ResultadoBusquedaDTO<EAplicacion> ResultadoBusquedaAplicacion { get; set; }
        public ResultadoBusquedaDTO<EAcceso> ResultadoBusquedaPerfil { get; set; }
        public EServicioResponse eServicioResponse { get; set; }
    }
}
