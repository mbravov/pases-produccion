﻿using System;

namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EPerfil
    {
        public string PerfilID { get; set; }
        public string AplicacionID { get; set; }
        public string CodigoCorellativo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public string EstadoDescripcion { get; set; }
        public string UsuarioRegistroID { get; set; }
        public string UsuarioModificacionID { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaModificacion { get; set; }


        public string UsuarioIdRegistro { get; set; }
        public string tipoOperacion { get; set; } //1 registro // 2 actualizar
    }
}
