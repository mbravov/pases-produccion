﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Entidades
{
    public class ObservacionBE
    {
        public int Compilacion { get; set; }
        public string CondicionesEspeciales { get; set; }
    }
}
