﻿using Agrobanco.SGPP.Comun;
using Agrobanco.SGPP.Comun.Laserfiche;
using Agrobanco.SGPP.COMUN;
using Agrobanco.SGPP.Datos;
using Agrobanco.SGPP.Entidades;
using Agrobanco.SGPP.Entidades.RegistroAprobacionPase;
using Agrobanco.SGPP.Entidades.RegistroPase;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SGPP.LogicaNegocio
{
    public class RegistroAprobacionPaseLogica
    {
        private readonly RegistroAprobacionPaseDatos regAproPaseDatos;
        private IHostingEnvironment _hostingEnv;
        private readonly EnvioCorreo _envioCorreo;
        public RegistroAprobacionPaseLogica(IHostingEnvironment env, ILogger logger)
        {
            regAproPaseDatos = new RegistroAprobacionPaseDatos();
            _envioCorreo = new EnvioCorreo(logger);
            _hostingEnv = env;
        }
        public List<RegAprobPaseFilterResponse> ListarPases(RegAprobPaseFilterRequest oRequest)
        {
            try
            {
                return regAproPaseDatos.ListarPases(oRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EstadosPaseBE> ListarEstados()
        {
            try
            {
                return regAproPaseDatos.ListarEstados();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TiposPlataformaBE> ListarTipoPlataforma()
        {
            try
            {
                return regAproPaseDatos.ListarTiposPlataforma();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<LibreriaBE> ListarLibrerias(int Tipo)
        {
            try
            {
                return regAproPaseDatos.ListarLibrerias(Tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> RegistrarPase(RegPaseRequest oRequest)
        {
            try
            {

                int CodPaseProduccion = await regAproPaseDatos.RegistrarPase(oRequest);
                if (CodPaseProduccion > 0)
                {
                    string CorreoAnalista = await regAproPaseDatos.ObtenerCorreoPorCodPaseProduccion(CodPaseProduccion);
                    EnviarCorreoDeRegistro(oRequest, CodPaseProduccion, CorreoAnalista);

                    List<string> Destinatarios = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.EjecutivoSistemas);
                    EnviarCorreoDerivacion(oRequest, CodPaseProduccion, Destinatarios);
                }

                return await Task.Run(() => CodPaseProduccion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> ActualizarPase(RegPaseRequest oRequest)
        {
            try
            {
                bool paseActualizado = false;

                paseActualizado = await regAproPaseDatos.ActualizarPase(oRequest);
                if (paseActualizado)
                {
                    string CorreoAnalista = await regAproPaseDatos.ObtenerCorreoPorCodPaseProduccion(oRequest.CodPaseProduccion);
                    EnviarCorreoDeRegistro(oRequest, oRequest.CodPaseProduccion, CorreoAnalista);

                    List<string> Destinatarios = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.EjecutivoSistemas);
                    EnviarCorreoDerivacion(oRequest, oRequest.CodPaseProduccion, Destinatarios);
                }

                return await Task.Run(() => paseActualizado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AnalistaBE> ListarAnalista()
        {
            try
            {
                return regAproPaseDatos.ListarAnalista();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PaseBE ObtenerPase(int codPase)
        {
            try
            {
                return regAproPaseDatos.ObtenerPase(codPase);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> ObtenerDatosFlujo(int codPase)
        {
            try
            {
                return regAproPaseDatos.ObtenerDatosFlujo(codPase);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ComentarioFlujoBE> ObtenerComentariosFlujo(int codPase)
        {
            try
            {
                return regAproPaseDatos.ObtenerComentariosFlujo(codPase);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ArchivoBE> ListarArchivos(int codPase)
        {
            try
            {
                return regAproPaseDatos.ListarArchivos(codPase);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ObjetoBE> ListarObjetos(int codPase)
        {
            try
            {
                return regAproPaseDatos.ListarObjetos(codPase);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObservacionBE ObtenerObservaciones(int codPase)
        {
            try
            {
                return regAproPaseDatos.ObtenerObservacion(codPase);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Sustento>> ListarSustentos(int CodPaseProduccion)
        {
            try
            {
                return await regAproPaseDatos.ObtenerSustentoPorCodPaseProduccion(CodPaseProduccion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Metodos Privados
        private async Task<RegPaseRequest> RegistrarEnLaserFiche(RegPaseRequest oRequest, int CodPaseProduccion)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            DocumentoModel DocumentoModelRequest = new DocumentoModel();
            DocumentoModel DocumentoModelResponse = new DocumentoModel();
            List<Documento> lstDocumento = new List<Documento>();
            try
            {
                oRequest.Sustentos?.ForEach(sustento => {
                    string base64 = sustento.Archivo.Split(',')[1];
                    lstDocumento.Add(new Documento
                    {
                        Extension = System.IO.Path.GetExtension(sustento.Nombre).Split('.')[1],
                        ArchivoBytes = base64,
                        Nombre = sustento.Nombre.Substring(0, sustento.Nombre.LastIndexOf('.')),
                        Folder = CodPaseProduccion.ToString()
                    });
                });

                if (lstDocumento?.Count > 0)
                {
                    DocumentoModelRequest.Folder = CodPaseProduccion.ToString();
                    DocumentoModelRequest.ListaDocumentos = lstDocumento;
                    DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                    for (var i = 0; i < DocumentoModelResponse.ListaDocumentos.Count; i++)
                    {
                        oRequest.Sustentos[i].CodLaserFiche = DocumentoModelResponse.ListaDocumentos[i].CodigoLaserfiche;
                    }
                };
                return await Task.Run(() => oRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        private void EnviarCorreoDeRegistro(RegPaseRequest oRequest, int CodPaseProduccion, string CorreoAnalista)
        {
            string DescripcionTipoPase = ObtenerDescripcionTipoPase(oRequest.TipoPase);

            string Asunto = $"Pase a producción de {DescripcionTipoPase} {CodPaseProduccion} CREADO ";
            string path = _hostingEnv.WebRootPath;
            string body = CuerpoCorreo.CuerpoCorreoRegistro(CodPaseProduccion,
                                                            DescripcionTipoPase,
                                                            oRequest.Descripcion,
                                                            oRequest.Ticket,
                                                            path);
            List<string> lstDestinatarios = new List<string>();
            lstDestinatarios.Add(CorreoAnalista);
            bool envioOK = _envioCorreo.EnviarCorreo(lstDestinatarios, null, body, Asunto);
        }
        private string ObtenerDescripcionTipoPase(int tipoPase)
        {
            string descripcionTipo = string.Empty;
            switch (tipoPase)
            {
                case 1:
                    descripcionTipo = TipoPaseDescripcion.AplicacionesWeb;
                    break;
                case 2:
                    descripcionTipo = TipoPaseDescripcion.AS400;
                    break;
                case 3:
                    descripcionTipo = TipoPaseDescripcion.PaginasWeb;
                    break;
                case 4:
                    descripcionTipo = TipoPaseDescripcion.Spring;
                    break;
            }
            return descripcionTipo;
        }

        private void EnviarCorreoDerivacion(RegPaseRequest oRequest, int CodPaseProduccion, List<string> Destinatarios)
        {
            string DescripcionTipoPase = ObtenerDescripcionTipoPase(oRequest.TipoPase);

            string Asunto = $"Pase a producción de {DescripcionTipoPase} {CodPaseProduccion} CREADO ";
            string path = _hostingEnv.WebRootPath;
            string body = CuerpoCorreo.CuerpoCorreoDerivacion(CodPaseProduccion,
                                                            DescripcionTipoPase,
                                                            oRequest.Descripcion,
                                                            oRequest.Ticket,
                                                            path);
            bool envioOK = _envioCorreo.EnviarCorreo(Destinatarios, null, body, Asunto);
        }

        public List<InformacionSoporteBE> ObtenerInformacionSoporte(int codPase)
        {
            try
            {
                return regAproPaseDatos.ObtenerInformacionSoporte(codPase);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> AprobarPase(ActualizacionPase request)
        {
            try
            {
                int estado = 1;
                string EnviadoA = string.Empty;
                string AprobadoPor = string.Empty;
                //var datosPase = regAproPaseDatos.ObtenerPase(request.CodPase);

                //logica estados
                switch (request.Perfil)
                {
                    case "EJS":
                        estado = EstadosPase.DerCalidad;
                        AprobadoPor = "el Ejecutivo de sistemas";
                        EnviadoA = "Calidad";
                        break;
                    case "CAL":
                        estado = EstadosPase.DerSoporte;
                        AprobadoPor = "Control de calidad";
                        EnviadoA = "Soporte";
                        break;
                    case "SOP":
                        estado = EstadosPase.Aprobado;
                        AprobadoPor = "Soporte";
                        break;
                }


                bool ActualizacionOk = await regAproPaseDatos.ActualizarEstadoPase(request, estado);
                if (ActualizacionOk)
                {
                    PaseBE oPase = regAproPaseDatos.ObtenerPase(request.CodPase);
                    string path = _hostingEnv.WebRootPath;

                    string Asunto = string.Empty;
                    string TipoPaseDesc = string.Empty;


                    switch (oPase.TipoPase)
                    {
                        case 1:
                            TipoPaseDesc = TipoPaseDescripcion.AplicacionesWeb;
                            break;
                        case 2:
                            TipoPaseDesc = TipoPaseDescripcion.AS400;
                            break;
                        case 3:
                            TipoPaseDesc = TipoPaseDescripcion.PaginasWeb;
                            break;
                        case 4:
                            TipoPaseDesc = TipoPaseDescripcion.Spring;
                            break;
                    }
                    if (estado == EstadosPase.DerCalidad)
                    {
                        //envio correo analista
                        string bodyAnalista = CuerpoCorreo.CuerpoCorreoAprobacion(oPase, path, estado,EnviadoA, true);
                        string CorreoAnalista = await regAproPaseDatos.ObtenerCorreoPorCodPaseProduccion(request.CodPase);
                        List<string> lstDestinatarios = new List<string>();
                        lstDestinatarios.Add(CorreoAnalista);

                        Asunto = "Pase a producción de "+TipoPaseDesc+" "+oPase.CodPaseProduccion+" APROBADO por "+AprobadoPor;

                        _envioCorreo.EnviarCorreo(lstDestinatarios, null, bodyAnalista, Asunto);

                        //envio correo calidad
                        string bodyCalidad = CuerpoCorreo.CuerpoCorreoAprobacion(oPase, path, estado, EnviadoA,false);
                        List<string> Destinatarios = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.Calidad);
                        _envioCorreo.EnviarCorreo(Destinatarios, null, bodyCalidad, Asunto);

                    }
                    else if (estado == EstadosPase.DerSoporte)
                    {
                        //envio correo analista
                        string bodyAnalista = CuerpoCorreo.CuerpoCorreoAprobacion(oPase, path, estado, EnviadoA, true);
                        string CorreoAnalista = await regAproPaseDatos.ObtenerCorreoPorCodPaseProduccion(request.CodPase);
                        List<string> lstDestinatarios = new List<string>();
                        lstDestinatarios.Add(CorreoAnalista);

                        Asunto = "Pase a producción de " + TipoPaseDesc + " " + oPase.CodPaseProduccion + " APROBADO por " + AprobadoPor;

                        _envioCorreo.EnviarCorreo(lstDestinatarios, null, bodyAnalista, Asunto);

                        //envio correo soporte
                        string bodySoporte = CuerpoCorreo.CuerpoCorreoAprobacion(oPase, path, estado, EnviadoA, false);
                        List<string> Destinatarios = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.Soporte);
                        _envioCorreo.EnviarCorreo(Destinatarios, null, bodySoporte, Asunto);

                    }
                    else if (estado == EstadosPase.Aprobado)
                    {
                        //envio correo a ejecutivo de sistemas y a calidad
                        string bodyAnalista = CuerpoCorreo.CuerpoCorreoAprobacion(oPase, path, estado, EnviadoA, false);
                        string CorreoAnalista = await regAproPaseDatos.ObtenerCorreoPorCodPaseProduccion(request.CodPase);
                        
                        List<string> DestinatariosEJS = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.EjecutivoSistemas);

                        List<string> lstDestinatarios = new List<string>();
                        lstDestinatarios.Add(CorreoAnalista);
                        lstDestinatarios.AddRange(DestinatariosEJS);

                        Asunto = "Pase a producción de " + TipoPaseDesc + " " + oPase.CodPaseProduccion + " PROCESADO";

                        _envioCorreo.EnviarCorreo(lstDestinatarios, null, bodyAnalista, Asunto);

                    }

                }

                return await Task.Run(() => ActualizacionOk);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<RespuestaActualizacion> RechazarPase(ActualizacionPase request)
        {
            try
            {
                RespuestaActualizacion respuesta = new RespuestaActualizacion();
                int Estado = EstadosPase.DevAnalista;
                string EstadoEnvioCorreo = string.Empty;
                respuesta.Procesado = false;
                bool ActualizacionOk = await regAproPaseDatos.ActualizarEstadoPase(request, Estado);
                if (ActualizacionOk)
                {
                    respuesta.Procesado = ActualizacionOk;
                    PaseBE datosPase = regAproPaseDatos.ObtenerPase(request.CodPase);
                    bool EnvioOK = await EnviarCorreoDeRechazoAsync(request, datosPase);
                    EstadoEnvioCorreo = EnvioOK ? "Correo enviado con exito." : "Ocurrio un error en el envio de correo.";
                    respuesta.Mensaje = $"Se ha rechazado correctamente el pase a producción. \n {EstadoEnvioCorreo}";
                }

                return await Task.Run(() => respuesta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<bool> EnviarCorreoDeRechazoAsync(ActualizacionPase request, PaseBE datosPase)
        {
            bool EnvioOk = false;
            List<string> lstDestinatarios = new List<string>();
            string Perfil = string.Empty;
            if (request.Perfil.Equals(ConstantesPerfil.EjecutivoSistemas)) Perfil = "el ejecutivo de sistemas";
            if (request.Perfil.Equals(ConstantesPerfil.Calidad)) Perfil = "control de calidad";
            if (request.Perfil.Equals(ConstantesPerfil.Soporte)) Perfil = "el operador";

            string CorreoAnalista = await regAproPaseDatos.ObtenerCorreoPorCodPaseProduccion(datosPase.CodPaseProduccion);
            string DescripcionTipoPase = ObtenerDescripcionTipoPase(datosPase.TipoPase);

            string Asunto = $"Pase a producción de {DescripcionTipoPase} {datosPase.CodPaseProduccion} ha sido RECHAZADO por {Perfil}";
            string path = _hostingEnv.WebRootPath;
            string body = CuerpoCorreo.CuerpoCorreoRechazo(datosPase,
                                                            DescripcionTipoPase,
                                                            path);

            if (request.Perfil.Equals(ConstantesPerfil.Soporte))
            {
                List<string> lstDestinatarioEJS = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.EjecutivoSistemas);
                List<string> lstDestinatarioCAL = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.Calidad);
                lstDestinatarios.AddRange(lstDestinatarioEJS);
                lstDestinatarios.AddRange(lstDestinatarioCAL);
            }

            lstDestinatarios.Add(CorreoAnalista);

            EnvioOk = _envioCorreo.EnviarCorreo(lstDestinatarios, null, body, Asunto);

            return await Task.Run(() => EnvioOk);
        }

        public async Task<RespuestaActualizacion> AnularPase(ActualizacionPase request)
        {
            try
            {
                string EstadoEnvioCorreo = string.Empty;
                RespuestaActualizacion respuesta = new RespuestaActualizacion();
                int estado = EstadosPase.Anulado;
                bool ActualizacionOk = await regAproPaseDatos.ActualizarEstadoPase(request, estado);
                if (ActualizacionOk)
                {
                    respuesta.Procesado = ActualizacionOk;
                    PaseBE datosPase = regAproPaseDatos.ObtenerPase(request.CodPase);
                    bool EnvioOK = await EnviarCorreoDeAnulacionAsync(request, datosPase);
                    EstadoEnvioCorreo = EnvioOK ? "Correo enviado con exito." : "Ocurrio un error en el envio de correo.";
                    respuesta.Mensaje = $"Se ha anulado correctamente el pase a producción. \n {EstadoEnvioCorreo}";
                }

                return await Task.Run(() => respuesta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<bool> EnviarCorreoDeAnulacionAsync(ActualizacionPase request, PaseBE datosPase)
        {
            bool EnvioOk = false;
            List<string> lstDestinatarios = new List<string>();
            string Perfil = string.Empty;
            if (request.Perfil.Equals(ConstantesPerfil.EjecutivoSistemas)) Perfil = "el ejecutivo de sistemas";
            if (request.Perfil.Equals(ConstantesPerfil.Calidad)) Perfil = "control de calidad";
            if (request.Perfil.Equals(ConstantesPerfil.Soporte)) Perfil = "el operador";

            string CorreoAnalista = await regAproPaseDatos.ObtenerCorreoPorCodPaseProduccion(datosPase.CodPaseProduccion);
            string DescripcionTipoPase = ObtenerDescripcionTipoPase(datosPase.TipoPase);

            string Asunto = $"Pase a producción de {DescripcionTipoPase} {datosPase.CodPaseProduccion} ha sido ANULADO por {Perfil}";
            string path = _hostingEnv.WebRootPath;
            string body = CuerpoCorreo.CuerpoCorreoAnulacion(datosPase,
                                                            DescripcionTipoPase,
                                                            path);

            if (request.Perfil.Equals(ConstantesPerfil.Soporte))
            {
                List<string> lstDestinatarioEJS = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.EjecutivoSistemas);
                List<string> lstDestinatarioCAL = await regAproPaseDatos.ObtenerDestinatariosPorPerfil(ConstantesPerfil.Calidad);
                lstDestinatarios.AddRange(lstDestinatarioEJS);
                lstDestinatarios.AddRange(lstDestinatarioCAL);
            }

            lstDestinatarios.Add(CorreoAnalista);

            EnvioOk = _envioCorreo.EnviarCorreo(lstDestinatarios, null, body, Asunto);

            return await Task.Run(() => EnvioOk);
        }

        #endregion
    }

}
