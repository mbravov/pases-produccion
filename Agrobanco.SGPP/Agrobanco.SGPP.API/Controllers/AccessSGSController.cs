﻿using Agrobanco.SGPP.Entidades.SGS;
using Agrobanco.SGPP.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGPP.API.Controllers
{
    [Route("api/sgs")]
    [ApiController]
    public class AccessSGSController : Controller
    {
        [HttpGet("usuario/LoadMantenimientoUsuario/{usuarioWeb}")]
        [ProducesResponseType(200, Type = typeof(List<EModulo>))]
        public object LoadMantenimientoUsuario([Required][FromHeader(Name = "Authorization")] string Authorization, [Required][FromHeader(Name = "Content-Type")] string Authorization2, string usuarioWeb)
        {
            try
            {
                ProxySGSAPILogica proxi = new ProxySGSAPILogica();
                //var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.LoadMantenimientoUsuario(usuarioWeb, Authorization);
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("modulo/obtenermodulos/{idPerfil}/{usuarioWeb}")]
        [ProducesResponseType(200, Type = typeof(List<EModulo>))]
        public object ObtenerModulos([Required][FromHeader(Name = "Authorization")] string Authorization, [Required][FromHeader(Name = "Content-Type")] string Authorization2, string idPerfil, string usuarioWeb)
        {
            try
            {
                ProxySGSAPILogica proxi = new ProxySGSAPILogica();
                //var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.ObtenerModulos(idPerfil, usuarioWeb, Authorization);
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("usuario/obtenerusuario")]
        [ProducesResponseType(200, Type = typeof(EloginResponse))]
        public object obtenerusuario([FromHeader] string Authorization, EUsuario request)
        {
            try
            {
                ProxySGSAPILogica proxi = new ProxySGSAPILogica();
                //var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.ObtenerUsuario(request, Authorization);
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("validate/token")]
        [ProducesResponseType(200, Type = typeof(IActionResult))]
        public IActionResult validate([Required][FromHeader(Name = "Authorization")] string Authorization, [Required][FromHeader(Name = "Content-Type")] string Authorization2)
        {
            try
            {
                IActionResult result;
                bool validate = true;
                ProxySGSAPILogica proxi = new ProxySGSAPILogica();
                //var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.validate(Authorization);
                if (response == "401" || response == "ex")
                {
                    validate = false;
                    result = new StatusCodeResult(401);
                }
                else
                {
                    result = new StatusCodeResult(200);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
