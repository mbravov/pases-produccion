﻿using Agrobanco.SGPP.Comun.Laserfiche;
using Agrobanco.SGPP.Datos.SqlServer.Acceso;
using Agrobanco.SGPP.Entidades;
using Agrobanco.SGPP.Entidades.RegistroAprobacionPase;
using Agrobanco.SGPP.Entidades.RegistroPase;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SGPP.Datos
{
    public class RegistroAprobacionPaseDatos
    {
        public string cadenaConexion;
        public RegistroAprobacionPaseDatos() { }
        public List<RegAprobPaseFilterResponse> ListarPases(RegAprobPaseFilterRequest oRequest)
        {
            
                this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
                RegAprobPaseFilterResponse objRequerimiento = null;
                List<RegAprobPaseFilterResponse> lstPase = new List<RegAprobPaseFilterResponse>();

                try
                {
                    using (SqlConnection conn = new SqlConnection(cadenaConexion))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("[dbo].[SP_PASE_LISTAR]", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;                            
                            cmd.Parameters.Add("@TipoPlataforma", SqlDbType.Int).Value = oRequest.CodTipoPlataforma;
                            cmd.Parameters.Add("@CodEstado", SqlDbType.Int).Value =  oRequest.CodEstado;
                            cmd.Parameters.Add("@FechaRegistroIni", SqlDbType.VarChar).Value = oRequest.FechaRegistroIni == null ? string.Empty : oRequest.FechaRegistroIni;
                            cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.VarChar).Value = oRequest.FechaRegistroFin == null ? string.Empty : oRequest.FechaRegistroFin;
                            

                        try
                            {
                                using (SqlDataReader reader = cmd.ExecuteReader())
                                {

                                    while (reader.Read())
                                    {
                                        objRequerimiento = new RegAprobPaseFilterResponse();
                                        if (!Convert.IsDBNull(reader["IdPases"])) objRequerimiento.IDPase = Convert.ToInt32(reader["IdPases"]);
                                        if (!Convert.IsDBNull(reader["Nombres"])) objRequerimiento.Nombre = Convert.ToString(reader["Nombres"]);
                                        if (!Convert.IsDBNull(reader["Descripcion"])) objRequerimiento.Descripcion = Convert.ToString(reader["Descripcion"]);
                                        if (!Convert.IsDBNull(reader["CreadoPor"])) objRequerimiento.CreadoPor = Convert.ToString(reader["CreadoPor"]);                                        
                                        if (!Convert.IsDBNull(reader["AprobadorPor"])) objRequerimiento.AprobadoPor = Convert.ToString(reader["AprobadorPor"]);
                                        if (!Convert.IsDBNull(reader["FechaRegistro"])) objRequerimiento.FechaRegistro = reader.GetDateTime("FechaRegistro");                                    
                                        if (!Convert.IsDBNull(reader["CodEstado"])) objRequerimiento.Estado = Convert.ToString(reader["CodEstado"]);
                                        if (!Convert.IsDBNull(reader["Pase"])) objRequerimiento.Pase = Convert.ToString(reader["Pase"]);
                                    lstPase.Add(objRequerimiento);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                throw ex;
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();

                            }
                        }
                    }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstPase;
        }

        public List<EstadosPaseBE> ListarEstados()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            EstadosPaseBE objEstado = null;
            List<EstadosPaseBE> lstEstado = new List<EstadosPaseBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_ESTADO_LISTAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                       
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objEstado = new EstadosPaseBE();
                                    if (!Convert.IsDBNull(reader["Id"])) objEstado.Id = Convert.ToInt32(reader["Id"]);
                                    if (!Convert.IsDBNull(reader["CodEstado"])) objEstado.CodEstado = Convert.ToString(reader["CodEstado"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objEstado.Descripcion = Convert.ToString(reader["Descripcion"]);

                                    lstEstado.Add(objEstado);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstEstado;
        }

        public List<TiposPlataformaBE> ListarTiposPlataforma()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            TiposPlataformaBE objEstado = null;
            List<TiposPlataformaBE> lstTipoPlataforma = new List<TiposPlataformaBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_TIPO_PLATAFORMA_LISTAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objEstado = new TiposPlataformaBE();
                                    if (!Convert.IsDBNull(reader["CodTipoPlataforma"])) objEstado.CodTipoPlataforma = Convert.ToInt32(reader["CodTipoPlataforma"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objEstado.Descripcion = Convert.ToString(reader["Descripcion"]);

                                    lstTipoPlataforma.Add(objEstado);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstTipoPlataforma;
        }

        public ObservacionBE ObtenerObservacion(int CodPaseProduccion)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ArchivoBE oArchivo = null;
            ObservacionBE oObservacion = new ObservacionBE();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    using (SqlCommand cmd = new SqlCommand("SP_OBSERVACION_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = CodPaseProduccion;

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                oObservacion = new ObservacionBE();
                                if (!Convert.IsDBNull(reader["Compilacion"])) oObservacion.Compilacion = Convert.ToInt32(reader["Compilacion"]);
                                if (!Convert.IsDBNull(reader["CondicionesEspeciales"])) oObservacion.CondicionesEspeciales = Convert.ToString(reader["CondicionesEspeciales"]);
  
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return oObservacion;
        }

        #region Registro del Pase a Producción
        public async Task<int> RegistrarPase(RegPaseRequest oRequest)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            int CodPaseProduccion;
            bool RegistroLaserFicheOK=false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                var transaccion = conn.BeginTransaction();
                try
                {
                    var objetos = new List<SqlDataRecord>();
                    var archivos = new List<SqlDataRecord>();
                    var sustentos = new List<SqlDataRecord>();
                    SqlMetaData Tipo = new SqlMetaData("Tipo", SqlDbType.Int);
                    SqlMetaData Nombre = new SqlMetaData("Nombre", SqlDbType.VarChar, 150);
                    SqlMetaData Descripcion = new SqlMetaData("Descripcion", SqlDbType.VarChar, 1500);
                    SqlMetaData Origen = new SqlMetaData("Origen", SqlDbType.VarChar, 500);
                    SqlMetaData Destino = new SqlMetaData("Destino", SqlDbType.VarChar, 500);
                    SqlMetaData Modulo = new SqlMetaData("Modulo", SqlDbType.Int);
                    SqlMetaData CodLaserFiche = new SqlMetaData("CodLaserFiche", SqlDbType.Int);


                    oRequest.Objetos?.ForEach(objeto => {
                        var row = new SqlDataRecord(Tipo, Nombre, Descripcion);
                        row.SetInt32(0, objeto.Tipo);
                        row.SetString(1, objeto.Nombre);
                        row.SetString(2, objeto.Descripcion);
                        objetos.Add(row);
                    });

                    objetos = (objetos.Count == 0) ? null : objetos;

                    oRequest.Archivos?.ForEach(archivo => {
                        var row = new SqlDataRecord(Nombre, Origen, Destino, Modulo);
                        row.SetString(0, archivo.Nombre);
                        row.SetString(1, archivo.Origen);
                        row.SetString(2, archivo.Destino);
                        row.SetInt32(3, archivo.Modulo);
                        archivos.Add(row);
                    });

                    archivos = (archivos.Count == 0) ? null : archivos;

                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_PASE_INSERTAR]", conn, transaccion))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@TipoPase", SqlDbType.Int).Value = oRequest.TipoPase;
                        cmd.Parameters.Add("@FechaPase", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("@Ticket", SqlDbType.VarChar).Value = oRequest.Ticket == null ? string.Empty : oRequest.Ticket;
                        cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar).Value = oRequest.Descripcion == null ? string.Empty : oRequest.Descripcion;
                        cmd.Parameters.Add("@UbicacionFuente", SqlDbType.VarChar).Value = oRequest.UbicacionFuente == null ? string.Empty : oRequest.UbicacionFuente;
                        if (oRequest.PaseInterno >= 0) 
                            cmd.Parameters.Add("@PaseInterno", SqlDbType.Bit).Value = oRequest.PaseInterno == 1 ? true : false;
                        else 
                            cmd.Parameters.Add("@PaseInterno", SqlDbType.Bit).Value = DBNull.Value;
                        cmd.Parameters.Add("@MotivoPase", SqlDbType.Int).Value = oRequest.MotivoPase;
                        cmd.Parameters.Add("@Compilacion", SqlDbType.Int).Value = oRequest.Compilacion;
                        cmd.Parameters.Add("@CondicionesEspeciales", SqlDbType.VarChar).Value = oRequest.CondicionesEspeciales == null ? string.Empty : oRequest.CondicionesEspeciales;
                        cmd.Parameters.Add("@LibreriaFuente", SqlDbType.Int).Value = oRequest.LibreriaFuente;
                        cmd.Parameters.Add("@LibreriaObjetoOtros", SqlDbType.Int).Value = oRequest.LibreriaObjetoOtros;
                        cmd.Parameters.Add("@LibreriaObjetoPFyLF", SqlDbType.Int).Value = oRequest.LibreriaObjetoPFyLF;
                        cmd.Parameters.Add("@Analista", SqlDbType.Int).Value = oRequest.Analista;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = oRequest.UsuarioRegistro == null ? string.Empty : oRequest.UsuarioRegistro;
                        cmd.Parameters.Add("@Comentario", SqlDbType.VarChar).Value = oRequest.Comentario == null ? string.Empty : oRequest.Comentario;
                        cmd.Parameters.Add("@TObjetos", SqlDbType.Structured).Value = objetos;
                        cmd.Parameters.Add("@TArchivos", SqlDbType.Structured).Value = archivos;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();

                        CodPaseProduccion = Convert.ToInt32(cmd.Parameters["@CodPaseProduccion"].Value);
                    }

                    //registrar en laserFiche
                    oRequest = await RegistrarEnLaserFiche(oRequest, CodPaseProduccion);
                    RegistroLaserFicheOK = true;
                    transaccion.Commit();
                    bool InsertarSustentos = await RegistrarSustentos(oRequest, CodPaseProduccion);

                }
                catch (Exception ex)
                {
                    transaccion.Rollback();
                    conn.Close();
                    conn.Dispose();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return await Task.Run(() => CodPaseProduccion);
        }

        public async Task<bool> ActualizarEstadoPase(ActualizacionPase actualizacionPase, int estado)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            bool ActualizacionOk;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                try
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_PASE_ACTUALIZAR_ESTADO]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPase", SqlDbType.Int).Value = actualizacionPase.CodPase;
                        cmd.Parameters.Add("@Comentario", SqlDbType.VarChar).Value = actualizacionPase.Comentario;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = actualizacionPase.UsuarioRegistro;
                        cmd.Parameters.Add("@Perfil", SqlDbType.VarChar).Value = actualizacionPase.Perfil;
                        cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = estado;
                        cmd.Parameters.Add("@ActualizacionOk", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();

                        ActualizacionOk = Convert.ToBoolean(cmd.Parameters["@ActualizacionOk"].Value);
                    }
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return await Task.Run(() => ActualizacionOk);
        }

        private async Task<RegPaseRequest> RegistrarEnLaserFiche(RegPaseRequest oRequest, int CodPaseProduccion)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            DocumentoModel DocumentoModelRequest = new DocumentoModel();
            DocumentoModel DocumentoModelResponse = new DocumentoModel();
            List<Documento> lstDocumento = new List<Documento>();
            try
            {
                oRequest.Sustentos?.ForEach(sustento => {
                    string base64 = sustento.Archivo.Split(',')[1];
                    lstDocumento.Add(new Documento
                    {
                        Extension = System.IO.Path.GetExtension(sustento.Nombre).Split('.')[1],
                        ArchivoBytes = base64,
                        Nombre = sustento.Nombre.Substring(0, sustento.Nombre.LastIndexOf('.')),
                        Folder = CodPaseProduccion.ToString()
                    });
                });

                if (lstDocumento?.Count > 0)
                {
                    DocumentoModelRequest.Folder = CodPaseProduccion.ToString();
                    DocumentoModelRequest.ListaDocumentos = lstDocumento;
                    DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                    for (var i = 0; i < DocumentoModelResponse.ListaDocumentos.Count; i++)
                    {
                        oRequest.Sustentos[i].CodLaserFiche = DocumentoModelResponse.ListaDocumentos[i].CodigoLaserfiche;
                    }
                };
                return await Task.Run(() => oRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public async Task<bool> ActualizarPase(RegPaseRequest oRequest)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            bool resultado;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                List<Sustento> sustentosExistentes = await ObtenerSustentoPorCodPaseProduccion(oRequest.CodPaseProduccion);

                conn.Open();
                var transaccion = conn.BeginTransaction();
                try
                {
                    var objetos = new List<SqlDataRecord>();
                    var archivos = new List<SqlDataRecord>();
                    var sustentos = new List<SqlDataRecord>();
                    SqlMetaData Tipo = new SqlMetaData("Tipo", SqlDbType.Int);
                    SqlMetaData Nombre = new SqlMetaData("Nombre", SqlDbType.VarChar, 150);
                    SqlMetaData Descripcion = new SqlMetaData("Descripcion", SqlDbType.VarChar, 1500);
                    SqlMetaData Origen = new SqlMetaData("Origen", SqlDbType.VarChar, 500);
                    SqlMetaData Destino = new SqlMetaData("Destino", SqlDbType.VarChar, 500);
                    SqlMetaData Modulo = new SqlMetaData("Modulo", SqlDbType.Int);
                    SqlMetaData CodLaserFiche = new SqlMetaData("CodLaserFiche", SqlDbType.Int);

                    oRequest.Objetos?.ForEach(objeto => {
                        var row = new SqlDataRecord(Tipo, Nombre, Descripcion);
                        row.SetInt32(0, objeto.Tipo);
                        row.SetString(1, objeto.Nombre);
                        row.SetString(2, objeto.Descripcion);
                        objetos.Add(row);
                    });

                    objetos = (objetos.Count == 0) ? null : objetos;

                    oRequest.Archivos?.ForEach(archivo => {
                        var row = new SqlDataRecord(Nombre, Origen, Destino, Modulo);
                        row.SetString(0, archivo.Nombre);
                        row.SetString(1, archivo.Origen);
                        row.SetString(2, archivo.Destino);
                        row.SetInt32(3, archivo.Modulo);
                        archivos.Add(row);
                    });

                    archivos = (archivos.Count == 0) ? null : archivos;

                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_PASE_ACTUALIZAR]", conn, transaccion))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = oRequest.CodPaseProduccion;
                        cmd.Parameters.Add("@FechaPase", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("@Ticket", SqlDbType.VarChar).Value = oRequest.Ticket == null ? string.Empty : oRequest.Ticket;
                        cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar).Value = oRequest.Descripcion == null ? string.Empty : oRequest.Descripcion;
                        cmd.Parameters.Add("@UbicacionFuente", SqlDbType.VarChar).Value = oRequest.UbicacionFuente == null ? string.Empty : oRequest.UbicacionFuente;
                      
                        if (oRequest.PaseInterno >= 0)
                            cmd.Parameters.Add("@PaseInterno", SqlDbType.Bit).Value = oRequest.PaseInterno == 1 ? true : false;
                        else
                            cmd.Parameters.Add("@PaseInterno", SqlDbType.Bit).Value = DBNull.Value;

                        cmd.Parameters.Add("@MotivoPase", SqlDbType.Int).Value = oRequest.MotivoPase;
                        cmd.Parameters.Add("@Compilacion", SqlDbType.Int).Value = oRequest.Compilacion;
                        cmd.Parameters.Add("@CondicionesEspeciales", SqlDbType.VarChar).Value = oRequest.CondicionesEspeciales == null ? string.Empty : oRequest.CondicionesEspeciales;
                        cmd.Parameters.Add("@LibreriaFuente", SqlDbType.Int).Value = oRequest.LibreriaFuente;
                        cmd.Parameters.Add("@LibreriaObjetoOtros", SqlDbType.Int).Value = oRequest.LibreriaObjetoOtros;
                        cmd.Parameters.Add("@LibreriaObjetoPFyLF", SqlDbType.Int).Value = oRequest.LibreriaObjetoPFyLF;
                        cmd.Parameters.Add("@Analista", SqlDbType.Int).Value = oRequest.Analista;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = oRequest.UsuarioRegistro == null ? string.Empty : oRequest.UsuarioRegistro;
                        cmd.Parameters.Add("@Comentario", SqlDbType.VarChar).Value = oRequest.Comentario == null ? string.Empty : oRequest.Comentario;
                        cmd.Parameters.Add("@TObjetos", SqlDbType.Structured).Value = objetos;
                        cmd.Parameters.Add("@TArchivos", SqlDbType.Structured).Value = archivos;
                        cmd.Parameters.Add("@Resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();

                        resultado = Convert.ToBoolean(cmd.Parameters["@Resultado"].Value);
                    }
                    
                    sustentosExistentes?.ForEach(sustento => EliminarArchivoLaserFiche(sustento.CodLaserFiche));

                    RegPaseRequest registroLaserFiche = await RegistrarEnLaserFiche(oRequest, oRequest.CodPaseProduccion);

                    transaccion.Commit();
                    bool InsertarSustentos = await RegistrarSustentos(oRequest, oRequest.CodPaseProduccion);
                }
                catch (Exception ex)
                {
                    transaccion.Rollback();
                    conn.Close();
                    conn.Dispose();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }


            }
            return await Task.Run(() => resultado);
        }

        private bool EliminarArchivoLaserFiche(int CodLaserFiche)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            Documento document = new Documento { CodigoLaserfiche = CodLaserFiche };

            var response = laserfiche.RemoveFile(document);
            return true;
        }
        public async Task<bool> RegistrarSustentos(RegPaseRequest oRequest, int codPaseProduccion)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            int Confirmacion;
            bool Respuesta = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                try
                {
                    var sustentos = new List<SqlDataRecord>();
                    SqlMetaData Nombre = new SqlMetaData("Nombre", SqlDbType.VarChar, 150);
                    SqlMetaData CodLaserFiche = new SqlMetaData("CodLaserFiche", SqlDbType.Int);

                    oRequest.Sustentos?.ForEach(sustento =>
                    {
                        var row = new SqlDataRecord(Nombre, CodLaserFiche);
                        row.SetString(0, sustento.Nombre);
                        row.SetInt32(1, sustento.CodLaserFiche);
                        sustentos.Add(row);
                    });

                    sustentos = (sustentos.Count == 0) ? null : sustentos;

                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_SUSTENTO_INSERTAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@codPaseProduccion", SqlDbType.Int).Value = codPaseProduccion;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = oRequest.UsuarioRegistro == null ? string.Empty : oRequest.UsuarioRegistro;
                        cmd.Parameters.Add("@TSustentos", SqlDbType.Structured).Value = sustentos;
                        cmd.Parameters.Add("@Confirmacion", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();

                        Confirmacion = Convert.ToInt32(cmd.Parameters["@CodPaseProduccion"].Value);
                        Respuesta = Confirmacion > 0 ? true : false;
                    }
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return await Task.Run(() => Respuesta);
        }
        public List<LibreriaBE> ListarLibrerias(int Tipo)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            LibreriaBE objLibreria = null;
            List<LibreriaBE> lstLibreria = new List<LibreriaBE>();

            try
            {
                string SP = string.Empty;

                switch (Tipo) {
                    case 1: SP = "[dbo].[SP_SGPP_LIBRERIA_FUENTE_OBTENER]"; break;
                    case 2: SP = "[dbo].[SP_SGPP_LIBRERIA_OBJETO_OBTENER]"; break;
                    case 3: SP = "[dbo].[SP_SGPP_LIBRERIA_OBJETO_PFLF_OBTENER]"; break;
                    case 4: SP = "[dbo].[SP_SGPP_MODULO_PFLF_OBTENER]"; break;
                }


                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(SP, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objLibreria = new LibreriaBE();
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objLibreria.Descripcion = Convert.ToString(reader["Descripcion"]);
                                    if (!Convert.IsDBNull(reader["Valor"])) objLibreria.Valor = Convert.ToInt32(reader["Valor"]);

                                    lstLibreria.Add(objLibreria);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstLibreria;
        }

        public List<AnalistaBE> ListarAnalista()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            AnalistaBE objAnalista = null;
            List<AnalistaBE> lstAnalista = new List<AnalistaBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_SGPP_ANALISTA_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objAnalista = new AnalistaBE();
                                    if (!Convert.IsDBNull(reader["CodUsuario"])) objAnalista.CodUsuario = Convert.ToInt32(reader["CodUsuario"]);
                                    if (!Convert.IsDBNull(reader["NombresApellidos"])) objAnalista.NombresApellidos = Convert.ToString(reader["NombresApellidos"]);

                                    lstAnalista.Add(objAnalista);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstAnalista;
        }

        #endregion

        public PaseBE ObtenerPase(int codPase)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            PaseBE oPase = null;
 
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                   
                    using (SqlCommand cmd = new SqlCommand("SP_PASE_OBTENER_POR_CODIGO", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = codPase;

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                oPase = new PaseBE();
                                if (!Convert.IsDBNull(reader["CodPaseProduccion"])) oPase.CodPaseProduccion = Convert.ToInt32(reader["CodPaseProduccion"]);
                                if (!Convert.IsDBNull(reader["Analista"])) oPase.Analista = Convert.ToInt32(reader["Analista"]);
                                if (!Convert.IsDBNull(reader["Ticket"])) oPase.Ticket = Convert.ToString(reader["Ticket"]);
                                if (!Convert.IsDBNull(reader["TipoPase"])) oPase.TipoPase = Convert.ToInt32(reader["TipoPase"]);
                                if (!Convert.IsDBNull(reader["Descripcion"])) oPase.Descripcion = Convert.ToString(reader["Descripcion"]);
                                if (!Convert.IsDBNull(reader["UbicacionFuente"])) oPase.UbicacionFuente = Convert.ToString(reader["UbicacionFuente"]);
                                if (!Convert.IsDBNull(reader["PaseInterno"])) oPase.PaseInterno = Convert.ToBoolean(reader["PaseInterno"]);
                                if (!Convert.IsDBNull(reader["LibreriaFuente"])) oPase.LibreriaFuente = Convert.ToInt32(reader["LibreriaFuente"]);
                                if (!Convert.IsDBNull(reader["LibreriaObjetoOtros"])) oPase.LibreriaObjetoOtros = Convert.ToInt32(reader["LibreriaObjetoOtros"]);
                                if (!Convert.IsDBNull(reader["LibreriaObjetoPFyLF"])) oPase.LibreriaObjetoPFyLF = Convert.ToInt32(reader["LibreriaObjetoPFyLF"]);
                                if (!Convert.IsDBNull(reader["MotivoPase"])) oPase.MotivoPase = Convert.ToInt32(reader["MotivoPase"]);
                                if (!Convert.IsDBNull(reader["Estado"])) oPase.Estado = Convert.ToInt32(reader["Estado"]);
                                if (!Convert.IsDBNull(reader["Comentario"])) oPase.Comentario = Convert.ToString(reader["Comentario"]);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return oPase;
        }

        public List<string> ObtenerDatosFlujo(int codPase)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            string DescripcionFujoPase = string.Empty;            
            List<string> lstDescripcionFujoPase = new List<string>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    using (SqlCommand cmd = new SqlCommand("SP_PASE_DATOS_FLUJO_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = codPase;

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                
                                if (!Convert.IsDBNull(reader["DescripcionFujoPase"])) DescripcionFujoPase = Convert.ToString(reader["DescripcionFujoPase"]);
                                lstDescripcionFujoPase.Add(DescripcionFujoPase);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstDescripcionFujoPase;
        }

        public List<ComentarioFlujoBE> ObtenerComentariosFlujo(int codPase)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ComentarioFlujoBE objComentario = null;
            List<ComentarioFlujoBE> lstComentario = new List<ComentarioFlujoBE>();


            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    using (SqlCommand cmd = new SqlCommand("SP_PASE_COMENTARIOS_FLUJO_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = codPase;

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                objComentario = new ComentarioFlujoBE();

                                if (!Convert.IsDBNull(reader["CodPaseProduccion"])) objComentario.CodPaseProduccion = Convert.ToInt32(reader["CodPaseProduccion"]);
                                if (!Convert.IsDBNull(reader["NombresApellidos"])) objComentario.NombresApellidos = Convert.ToString(reader["NombresApellidos"]);
                                if (!Convert.IsDBNull(reader["Fecha"])) objComentario.Fecha = Convert.ToDateTime(reader["Fecha"]);
                                if (!Convert.IsDBNull(reader["Comentario"])) objComentario.Comentario = Convert.ToString(reader["Comentario"]);

                                lstComentario.Add(objComentario);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstComentario;
        }

        public async Task<List<string>> ObtenerDestinatariosPorPerfil(string Perfil)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            List<string> lstDestinatarios = new List<string>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_USUARIO_LISTAR_CORREO_PORPERFIL]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Perfil", SqlDbType.VarChar).Value = Perfil;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    if (!Convert.IsDBNull(reader["CorreoElectronico"]))
                                        lstDestinatarios.Add(Convert.ToString(reader["CorreoElectronico"]));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return await Task.Run(() => lstDestinatarios);
        }

        public async Task<string> ObtenerCorreoPorCodPaseProduccion(int CodPaseProduccion)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            string CorreoAnalista = string.Empty;
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_USUARIO_OBTENER_CORREO_POR_CODPASEPRODUCCION]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = CodPaseProduccion;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    if (!Convert.IsDBNull(reader["CorreoElectronico"]))
                                        CorreoAnalista = (Convert.ToString(reader["CorreoElectronico"]));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return await Task.Run(() => CorreoAnalista);
        }

        public List<ArchivoBE> ListarArchivos(int codPase)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ArchivoBE oArchivo = null;
            List<ArchivoBE> lstArchivos = new List<ArchivoBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    using (SqlCommand cmd = new SqlCommand("SP_ARCHIVO_LISTAR_POR_CODIGO", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = codPase;

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                oArchivo = new ArchivoBE();
                                if (!Convert.IsDBNull(reader["CodArchivo"])) oArchivo.CodArchivo = Convert.ToInt32(reader["CodArchivo"]);
                                if (!Convert.IsDBNull(reader["Nombre"])) oArchivo.Nombre = Convert.ToString(reader["Nombre"]);
                                if (!Convert.IsDBNull(reader["Origen"])) oArchivo.Origen = Convert.ToString(reader["Origen"]);
                                if (!Convert.IsDBNull(reader["Destino"])) oArchivo.Destino = Convert.ToString(reader["Destino"]);
                                if (!Convert.IsDBNull(reader["Modulo"])) oArchivo.Modulo = Convert.ToInt32(reader["Modulo"]);

                                lstArchivos.Add(oArchivo);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstArchivos;
        }

        public List<ObjetoBE> ListarObjetos(int codPase)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ObjetoBE oObjeto = null;
            List<ObjetoBE> lstObjetos = new List<ObjetoBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    using (SqlCommand cmd = new SqlCommand("SP_OBJETO_LISTAR_POR_CODIGO", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = codPase;

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                oObjeto = new ObjetoBE();
                                if (!Convert.IsDBNull(reader["CodObjeto"])) oObjeto.CodObjeto = Convert.ToInt32(reader["CodObjeto"]);
                                if (!Convert.IsDBNull(reader["Tipo"])) oObjeto.Tipo = Convert.ToInt32(reader["Tipo"]);
                                if (!Convert.IsDBNull(reader["Nombre"])) oObjeto.Nombre = Convert.ToString(reader["Nombre"]);
                                if (!Convert.IsDBNull(reader["Descripcion"])) oObjeto.Descripcion = Convert.ToString(reader["Descripcion"]);

                                lstObjetos.Add(oObjeto);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstObjetos;
        }

        public async Task<List<Sustento>> ObtenerSustentoPorCodPaseProduccion(int CodPaseProduccion)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            List<Sustento> lstSustento = new List<Sustento>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_SUSTENTO_LISTAR_POR_CODIGO]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodPaseProduccion", SqlDbType.Int).Value = CodPaseProduccion;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    lstSustento.Add(new Sustento
                                    {
                                        CodLaserFiche = Convert.ToInt32(reader["CodLaserFiche"]),
                                        Nombre = Convert.ToString(reader["Nombre"])
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return await Task.Run(() => lstSustento);
        }

        public List<InformacionSoporteBE> ObtenerInformacionSoporte(int codPase)
        {
            cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            List<InformacionSoporteBE> lstInformacion = new List<InformacionSoporteBE>();
            List<ObtenerInformacionSoporteBE> lstObtenerInformacion = new List<ObtenerInformacionSoporteBE>();
            ObtenerInformacionSoporteBE ObtenerInformacion;
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    using (SqlCommand cmd = new SqlCommand("SP_INFORMACION_SOPORTE_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@TipoPase", SqlDbType.Int).Value = codPase;

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ObtenerInformacion = new ObtenerInformacionSoporteBE();
                                if (!Convert.IsDBNull(reader["Titulo"])) ObtenerInformacion.Titulo = Convert.ToString(reader["Titulo"]);
                                if (!Convert.IsDBNull(reader["Item"])) ObtenerInformacion.Item = Convert.ToString(reader["Item"]);
                                lstObtenerInformacion.Add(ObtenerInformacion);
                            }
                        }
                    }
                }

                var Titulos = lstObtenerInformacion
                    .GroupBy(informacion => informacion.Titulo)
                    .Select(group => group.First()).ToList();

                Titulos?.ForEach(x => {
                    InformacionSoporteBE InformacionSoporte = new InformacionSoporteBE();
                    InformacionSoporte.Titulo = x.Titulo;

                    var Items = from info in lstObtenerInformacion
                                where info.Titulo.Equals(x.Titulo)
                                select new Item { Descripcion = info.Item };

                    InformacionSoporte.Item = Items.ToList();

                    lstInformacion.Add(InformacionSoporte);
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstInformacion;
        }

    }
}
