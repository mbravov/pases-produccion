export interface Pase{
    codPaseProduccion: number;
    analista: number;
    ticket: string;
    tipoPase: number;
    descripcion: string;
    ubicacionFuente: string;
    paseInterno: boolean;
    libreriaFuente: number;
    libreriaObjetoOtros: number;
    libreriaObjetoPFyLF: number;
    motivoPase: number;
    estado: number;
    comentario: string;
}