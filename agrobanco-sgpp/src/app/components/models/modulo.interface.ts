export interface Modulo{
    action : string,
    controller: string,
    correlativoOpcion: string,
    descripcionAplicacion: string,
    estadoOpcion: string,
    idAplicacion: string,
    idOpcion: string,
    idPerfil: string,
    idRelacion: string,
    nombreAplicacion: string,
    nombreOpcion: string,
    nombrePerfil: string,
    tipoIconoCodigo: string,
    tipoIconoDescripcion: string,
    tipoOpcion: string,
    urlAplicacion: string,
    urlOpcion: string
}