export interface RegistroPaseConsulta {
    IDPase : string,
    Nombre : string,
    Descripcion : string,
    CreadoPor : string,
    AprobadorPor : string,
    FechaRegistro : Date,
    Estado : string,    
    Pase : string 
}
