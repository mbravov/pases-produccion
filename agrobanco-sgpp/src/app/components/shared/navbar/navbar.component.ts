import { Component, OnInit } from '@angular/core';
import { DomService } from 'src/app/services/dom.service';
import { SecurityService } from 'src/app/services/security.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  nombreUsuario!: string;
  constructor(public domService : DomService, public secService: SecurityService) { 

  }

  ngOnInit(): void {
    this.nombreUsuario = this.secService.leerNombreUsuario();
  }
  
  MenuOpenClick(): void{
    this.domService.ShowSideBar();
  }

}
