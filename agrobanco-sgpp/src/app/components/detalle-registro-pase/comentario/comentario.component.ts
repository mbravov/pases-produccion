import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Perfiles } from 'src/app/helpers/constantes';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { comentarioFlujo } from '../../models/comentario-flujo.interface';
import { Pase } from '../../models/pase.interface';

@Component({
    selector: 'app-comentario',
    templateUrl: './comentario.component.html',
    styleUrls: ['./comentario.component.css']
})
export class ComentarioComponent implements OnInit,OnChanges {
  formBuilder: FormBuilder = new FormBuilder();
  comentarios! : FormGroup;
  CondicionesEspeciales:any = '';
  @Input() pase!: Pase;
  isRegistro: boolean= true;
  height: string ="250px"

  lDatosComentarios: any  [] = [];
  datosComentarios!: string;

  cPerfil: any = '';
  eANA: Perfiles = Perfiles.ANA;

  constructor(private registroService : RegistroPaseService) { }
  ngOnChanges(changes: SimpleChanges): void {
    if(this.pase){
      this.isRegistro=true;
      this.height = "250px";
      this.obtenerDatosComentarios(this.pase.codPaseProduccion);
    }else{
        this.isRegistro=false;
        this.height="100px";
    }
  }

  ngOnInit(): void {
    this.comentarios = this.formBuilder.group({
      comentario: ['', [Validators.required]]
    });
  }

  get f(){
    return this.comentarios.controls;
  }

  obtenerDatosComentarios(codPase: number):void{

    this.registroService.ObtenerComentariosFlujo(codPase).subscribe((result : any)=>{
      this.lDatosComentarios = result.response as comentarioFlujo[];

      this.datosComentarios = "";
      for(let i=0; i<this.lDatosComentarios.length; i++){

        let AmPm = parseInt(moment(this.lDatosComentarios[i].fecha).format("HH")) >=12 ?"PM":"AM";

        this.datosComentarios += 
                this.lDatosComentarios[i].nombresApellidos +" - "+
                moment(this.lDatosComentarios[i].fecha).format("DD/MM/YYYY hh:mm:ss")+" "+AmPm
                +"\n"+this.lDatosComentarios[i].comentario+"\n \n";
      }

      this.comentarios.controls.comentario.setValue(this.datosComentarios);

    });


    

  }

}