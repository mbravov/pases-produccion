import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { TipoPlataforma } from 'src/app/helpers/constantes';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { Pase } from '../../models/pase.interface';

@Component({
    selector: 'app-informacion-soporte',
    templateUrl: './informacion-soporte.component.html',
    styleUrls: ['./informacion-soporte.component.css']
})
export class InformacionSoporteComponent implements OnInit, OnChanges  {
  
  title: string = '';
  subtitle: string = '';
  lstInformacion: any[] = [];

  @Input() tipoPase!: number;

  constructor(private registroservice : RegistroPaseService) { 
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.tipoPase){
      this.setTitle();
      this.obtenerInfoSoporte();
    }

  }

  ngOnInit(): void {
    
  }

  setTitle(){
    switch(this.tipoPase){
      case TipoPlataforma.AplicacionWeb:
      case TipoPlataforma.AS400_IBS:
        this.title = 'Para ser llenado por Soporte Técnico';
        this.subtitle = 'Para ingresar a Control de Calidad se requiere adjuntar la siguiente documentación:';
        break;
      case TipoPlataforma.PaginasWeb:
      case TipoPlataforma.Spring:
        this.title = 'Para cerrar el Pase a Producción';
        break;
    }
  }

  obtenerInfoSoporte():void{
    this.registroservice.ObtenerInfoSoporte(this.tipoPase).subscribe((result : any)=>{
      this.lstInformacion = result.response;
     });

  }


}