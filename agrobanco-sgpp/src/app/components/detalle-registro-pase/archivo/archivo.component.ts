import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Estados, Perfiles, TipoPlataforma } from 'src/app/helpers/constantes';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { ArchivoDetalle } from '../../models/archivo-detalle.interface';
import { Pase } from '../../models/pase.interface';

@Component({
    selector: 'app-archivo',
    templateUrl: './archivo.component.html',
    styleUrls: ['./archivo.component.css']
})
export class ArchivoComponent implements OnInit, OnChanges, OnDestroy {

  loading: boolean = false;
  formBuilder: FormBuilder = new FormBuilder();
  archivos! : FormGroup;
  lstModulos: any[] = [];
  lstArchivos: ArchivoDetalle[] = [];

  @Input() tipoPase!: number;
  @Input() pase!: Pase;

  eAplicacionWeb: TipoPlataforma = TipoPlataforma.AplicacionWeb;
  eAS400_IBS: TipoPlataforma = TipoPlataforma.AS400_IBS;
  ePaginasWeb: TipoPlataforma = TipoPlataforma.PaginasWeb;
  eSpring: TipoPlataforma = TipoPlataforma.Spring;

  lstModulosSubs!: Subscription;
  lstArchivosSubs!: Subscription;

  eANA: Perfiles = Perfiles.ANA;

  cperfil!: string;

  constructor(
    private registroPaseService: RegistroPaseService
  ) { }
  
  ngOnChanges(changes: SimpleChanges): void {
    if(this.tipoPase && this.tipoPase === this.eSpring){
      this.cargarModulos();
    }

    if(this.pase){
      this.cargarArchivos();
    }
  }

  ngOnInit(): void {
    this.cperfil = localStorage.getItem('perfil')!;

    this.archivos = this.formBuilder.group({
      Archivo1: [''],
      Archivo2: [''],
      Archivo3: [''],
      Archivo4: [''],
      Archivo5: [''],
      Archivo6: [''],

      Archivo1_PaginaWeb: [''],
      Destino1_PaginaWeb: [''],
      Archivo2_PaginaWeb: [''],
      Destino2_PaginaWeb: [''],
      Archivo3_PaginaWeb: [''],
      Destino3_PaginaWeb: [''],
      Archivo4_PaginaWeb: [''],
      Destino4_PaginaWeb: [''],
      Archivo5_PaginaWeb: [''],
      Destino5_PaginaWeb: [''],
      Archivo6_PaginaWeb: [''],
      Destino6_PaginaWeb: [''],
      Archivo7_PaginaWeb: [''],
      Destino7_PaginaWeb: [''],
      Archivo8_PaginaWeb: [''],
      Destino8_PaginaWeb: [''],
      Archivo9_PaginaWeb: [''],
      Destino9_PaginaWeb: [''],
      Archivo10_PaginaWeb: [''],
      Destino10_PaginaWeb: [''],
      Archivo11_PaginaWeb: [''],
      Destino11_PaginaWeb: [''],
      Archivo12_PaginaWeb: [''],
      Destino12_PaginaWeb: [''],
      Archivo13_PaginaWeb: [''],
      Destino13_PaginaWeb: [''],
      Archivo14_PaginaWeb: [''],
      Destino14_PaginaWeb: [''],
      Archivo15_PaginaWeb: [''],
      Destino15_PaginaWeb: [''],
      Archivo16_PaginaWeb: [''],
      Destino16_PaginaWeb: [''],
      Archivo17_PaginaWeb: [''],
      Destino17_PaginaWeb: [''],
      Archivo18_PaginaWeb: [''],
      Destino18_PaginaWeb: [''],
      Archivo19_PaginaWeb: [''],
      Destino19_PaginaWeb: [''],
      Archivo20_PaginaWeb: [''],
      Destino20_PaginaWeb: [''],
      Archivo21_PaginaWeb: [''],
      Destino21_PaginaWeb: [''],
      Archivo22_PaginaWeb: [''],
      Destino22_PaginaWeb: [''],
      Archivo23_PaginaWeb: [''],
      Destino23_PaginaWeb: [''],
      Archivo24_PaginaWeb: [''],
      Destino24_PaginaWeb: [''],
      Archivo25_PaginaWeb: [''],
      Destino25_PaginaWeb: [''],
      Archivo26_PaginaWeb: [''],
      Destino26_PaginaWeb: [''],
      Archivo27_PaginaWeb: [''],
      Destino27_PaginaWeb: [''],
      Archivo28_PaginaWeb: [''],
      Destino28_PaginaWeb: [''],
      Archivo29_PaginaWeb: [''],
      Destino29_PaginaWeb: [''],
      Archivo30_PaginaWeb: [''],
      Destino30_PaginaWeb: [''],

      Modulo1: [''],
      Fuente1_Spring: [''],
      Destino1_Spring: [''],
      Modulo2: [''],
      Fuente2_Spring: [''],
      Destino2_Spring: [''],
      Modulo3: [''],
      Fuente3_Spring: [''],
      Destino3_Spring: [''],
      Modulo4: [''],
      Fuente4_Spring: [''],
      Destino4_Spring: [''],
    });
  }

  get f(){
    return this.archivos.controls;
  }

  cargarArchivos(){
    this.loading = true;

    this.lstArchivosSubs = this.registroPaseService.ObtenerListaArchivos(this.pase.codPaseProduccion)
      .subscribe(
        data => {
          this.lstArchivos = data.response as ArchivoDetalle[];

          if(this.pase.tipoPase === TipoPlataforma.AplicacionWeb){
            this.setArchivosAplicacionWeb();
          }else if(this.pase.tipoPase === TipoPlataforma.PaginasWeb){
            this.setArchivosPaginaWeb();
          }else if(this.pase.tipoPase === TipoPlataforma.Spring){
            this.setArchivosSpring();
          }

        },
        err => {
          console.log(err);
        },
        () => {
          this.loading = false;
        }
      )
  }

  private setArchivosSpring(){
    for(let i = 0; i< this.lstArchivos.length; i++){
      if(i == 0){
        this.f.Modulo1.setValue(this.lstArchivos[i].modulo);
        this.f.Fuente1_Spring.setValue(this.lstArchivos[i].origen);
        this.f.Destino1_Spring.setValue(this.lstArchivos[i].destino);
      }
      if(i == 1){
        this.f.Modulo2.setValue(this.lstArchivos[i].modulo);
        this.f.Fuente2_Spring.setValue(this.lstArchivos[i].origen);
        this.f.Destino2_Spring.setValue(this.lstArchivos[i].destino);
      }
      if(i == 2){
        this.f.Modulo3.setValue(this.lstArchivos[i].modulo);
        this.f.Fuente3_Spring.setValue(this.lstArchivos[i].origen);
        this.f.Destino3_Spring.setValue(this.lstArchivos[i].destino);
      }
      if(i == 3){
        this.f.Modulo4.setValue(this.lstArchivos[i].modulo);
        this.f.Fuente4_Spring.setValue(this.lstArchivos[i].origen);
        this.f.Destino4_Spring.setValue(this.lstArchivos[i].destino);
      }
    }

    if(this.cperfil !== this.eANA || this.pase.estado !== Estados.DevAnalista){
      this.f.Modulo1.disable();
      this.f.Fuente1_Spring.disable();
      this.f.Destino1_Spring.disable();

      this.f.Modulo2.disable();
      this.f.Fuente2_Spring.disable();
      this.f.Destino2_Spring.disable();

      this.f.Modulo3.disable();
      this.f.Fuente3_Spring.disable();
      this.f.Destino3_Spring.disable();

      this.f.Modulo4.disable();
      this.f.Fuente4_Spring.disable();
      this.f.Destino4_Spring.disable();
    }

  }

  private setArchivosPaginaWeb(){

    for(let i = 0; i< this.lstArchivos.length; i++){
      if(i == 0){
        this.f.Archivo1_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino1_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 1){
        this.f.Archivo2_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino2_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 2){
        this.f.Archivo3_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino3_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 3){
        this.f.Archivo4_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino4_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 4){
        this.f.Archivo5_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino5_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 5){
        this.f.Archivo6_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino6_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 6){
        this.f.Archivo7_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino7_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 7){
        this.f.Archivo8_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino8_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 8){
        this.f.Archivo9_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino9_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 9){
        this.f.Archivo10_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino10_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 10){
        this.f.Archivo11_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino11_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 11){
        this.f.Archivo12_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino12_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 12){
        this.f.Archivo13_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino13_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 13){
        this.f.Archivo14_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino14_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 14){
        this.f.Archivo15_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino15_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 15){
        this.f.Archivo16_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino16_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 16){
        this.f.Archivo17_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino17_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 17){
        this.f.Archivo18_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino18_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 18){
        this.f.Archivo19_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino19_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 19){
        this.f.Archivo20_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino20_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 20){
        this.f.Archivo21_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino21_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 21){
        this.f.Archivo22_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino22_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 22){
        this.f.Archivo23_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino23_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 23){
        this.f.Archivo24_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino24_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 24){
        this.f.Archivo25_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino25_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 25){
        this.f.Archivo26_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino26_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 26){
        this.f.Archivo27_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino27_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 27){
        this.f.Archivo28_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino28_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 28){
        this.f.Archivo29_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino29_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
      if(i == 29){
        this.f.Archivo30_PaginaWeb.setValue(this.lstArchivos[i].nombre);
        this.f.Destino30_PaginaWeb.setValue(this.lstArchivos[i].destino);
      }
    }

    if(this.cperfil !== this.eANA || this.pase.estado !== Estados.DevAnalista){
      this.f.Archivo1_PaginaWeb.disable();
      this.f.Destino1_PaginaWeb.disable();
      this.f.Archivo2_PaginaWeb.disable();
      this.f.Destino2_PaginaWeb.disable();
      this.f.Archivo3_PaginaWeb.disable();
      this.f.Destino3_PaginaWeb.disable();
      this.f.Archivo4_PaginaWeb.disable();
      this.f.Destino4_PaginaWeb.disable();
      this.f.Archivo5_PaginaWeb.disable();
      this.f.Destino5_PaginaWeb.disable();
      this.f.Archivo6_PaginaWeb.disable();
      this.f.Destino6_PaginaWeb.disable();
      this.f.Archivo7_PaginaWeb.disable();
      this.f.Destino7_PaginaWeb.disable();
      this.f.Archivo8_PaginaWeb.disable();
      this.f.Destino8_PaginaWeb.disable();
      this.f.Archivo9_PaginaWeb.disable();
      this.f.Destino9_PaginaWeb.disable();
      this.f.Archivo10_PaginaWeb.disable();
      this.f.Destino10_PaginaWeb.disable();
      this.f.Archivo11_PaginaWeb.disable();
      this.f.Destino11_PaginaWeb.disable();
      this.f.Archivo12_PaginaWeb.disable();
      this.f.Destino12_PaginaWeb.disable();
      this.f.Archivo13_PaginaWeb.disable();
      this.f.Destino13_PaginaWeb.disable();
      this.f.Archivo14_PaginaWeb.disable();
      this.f.Destino14_PaginaWeb.disable();
      this.f.Archivo15_PaginaWeb.disable();
      this.f.Destino15_PaginaWeb.disable();
      this.f.Archivo16_PaginaWeb.disable();
      this.f.Destino16_PaginaWeb.disable();
      this.f.Archivo17_PaginaWeb.disable();
      this.f.Destino17_PaginaWeb.disable();
      this.f.Archivo18_PaginaWeb.disable();
      this.f.Destino18_PaginaWeb.disable();
      this.f.Archivo19_PaginaWeb.disable();
      this.f.Destino19_PaginaWeb.disable();
      this.f.Archivo20_PaginaWeb.disable();
      this.f.Destino20_PaginaWeb.disable();
      this.f.Archivo21_PaginaWeb.disable();
      this.f.Destino21_PaginaWeb.disable();
      this.f.Archivo22_PaginaWeb.disable();
      this.f.Destino22_PaginaWeb.disable();
      this.f.Archivo23_PaginaWeb.disable();
      this.f.Destino23_PaginaWeb.disable();
      this.f.Archivo24_PaginaWeb.disable();
      this.f.Destino24_PaginaWeb.disable();
      this.f.Archivo25_PaginaWeb.disable();
      this.f.Destino25_PaginaWeb.disable();
      this.f.Archivo26_PaginaWeb.disable();
      this.f.Destino26_PaginaWeb.disable();
      this.f.Archivo27_PaginaWeb.disable();
      this.f.Destino27_PaginaWeb.disable();
      this.f.Archivo28_PaginaWeb.disable();
      this.f.Destino28_PaginaWeb.disable();
      this.f.Archivo29_PaginaWeb.disable();
      this.f.Destino29_PaginaWeb.disable();
      this.f.Archivo30_PaginaWeb.disable();
      this.f.Destino30_PaginaWeb.disable();
    }

  }

  private setArchivosAplicacionWeb(){
    for(let i = 0; i < this.lstArchivos.length; i++){
      if(i == 0){
        this.f.Archivo1.setValue(this.lstArchivos[i].nombre);
      }

      if(i == 1){
        this.f.Archivo2.setValue(this.lstArchivos[i].nombre);
      }

      if(i == 2){
        this.f.Archivo3.setValue(this.lstArchivos[i].nombre);
      }

      if(i == 3){
        this.f.Archivo4.setValue(this.lstArchivos[i].nombre);
      }

      if(i == 4){
        this.f.Archivo5.setValue(this.lstArchivos[i].nombre);
      }

      if(i == 5){
        this.f.Archivo6.setValue(this.lstArchivos[i].nombre);
      }
    }

    if(this.cperfil !== this.eANA || this.pase.estado !== Estados.DevAnalista){
      this.f.Archivo1.disable();
      this.f.Archivo2.disable();
      this.f.Archivo3.disable();
      this.f.Archivo4.disable();
      this.f.Archivo5.disable();
      this.f.Archivo6.disable();
    }
  }

  cargarModulos(){
    this.loading = true;
    this.lstModulosSubs = this.registroPaseService.ObtenerListaLibrerias(4)
        .subscribe(
          data => {
            this.lstModulos = data.response;
          },
          err => {
            console.log(err);
          },
          () => {
            this.loading = false;
          }
        )
  }

  ngOnDestroy(): void {
    this.lstModulosSubs?.unsubscribe();
    this.lstArchivosSubs?.unsubscribe();
  }

}