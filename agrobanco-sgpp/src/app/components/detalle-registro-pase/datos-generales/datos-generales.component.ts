import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { Estados, Perfiles, TipoPlataforma } from 'src/app/helpers/constantes';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { Analista } from '../../models/analistas.interface';
import { Libreria } from '../../models/librerias.interface';
import { Pase } from '../../models/pase.interface';

@Component({
    selector: 'app-datos-generales',
    templateUrl: './datos-generales.component.html',
    styleUrls: ['./datos-generales.component.css']
})
export class DatosGeneralesComponent implements OnInit, OnChanges  {

  datosGenerales! : FormGroup;

  lFuentes: any [] = [];
  lObjetos: any [] = [];
  lObjetosPFyLF: any [] = [];
  lAnalistas: any [] = [];

  @Input() tipoPase: number = 0;
  @Input() pase!: Pase;

  eAplicacionWeb = TipoPlataforma.AplicacionWeb;
  eAS400_IBS = TipoPlataforma.AS400_IBS;
  ePaginasWeb = TipoPlataforma.PaginasWeb;
  eSpring = TipoPlataforma.Spring;

  eANA: Perfiles = Perfiles.ANA;

  constructor(
    private formBuilder: FormBuilder,
    private registroPaseService : RegistroPaseService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.tipoPase){
      this.listarAnalistas();
      this.cargarFuentesyObjetos();
      this.configValidations();
    }
    
    if(this.pase){
      this.setDatosGenerales();
    }
    
  }
  
  ngOnInit(): void {  
    this.initFormulario();
    this.configValidations();
  }

  private initFormulario(){
    this.datosGenerales = this.formBuilder.group({
      analista: [null, [Validators.required]],
      ticket: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      fuente: [''],
      paseInterno: ['1'],
      libreriaFuente: [null],
      libreriaObjeto: [null],
      libreriaObjetoPFLF: [null],
      motivoPase: ['1']
    });
  }

  private setDatosGenerales(){
    
      this.f.analista.setValue(this.pase.analista);
      this.f.descripcion.setValue(this.pase.descripcion);
      this.f.ticket.setValue(this.pase.ticket);

      const perfil = localStorage.getItem('perfil')!;
      
      if(perfil !== this.eANA || this.pase.estado !== Estados.DevAnalista){
        this.f.analista.disable();
        this.f.descripcion.disable();
        this.f.ticket.disable();
      }

      switch(this.pase.tipoPase){
        case TipoPlataforma.AplicacionWeb:
        case TipoPlataforma.PaginasWeb:
        case TipoPlataforma.Spring:
          this.f.fuente.setValue(this.pase.ubicacionFuente);
          this.f.paseInterno.setValue(this.pase.paseInterno ? '1' : '0');
          
          if(perfil !== this.eANA ||this.pase.estado !== Estados.DevAnalista){
            this.f.fuente.disable();
            this.f.paseInterno.disable();
          }

          break;
        case TipoPlataforma.AS400_IBS:
          this.f.libreriaFuente.setValue(this.pase.libreriaFuente);
          this.f.libreriaObjeto.setValue(this.pase.libreriaObjetoOtros);
          this.f.libreriaObjetoPFLF.setValue(this.pase.libreriaObjetoPFyLF);
          this.f.motivoPase.setValue(String(this.pase.motivoPase));
          
          if(perfil !== this.eANA || this.pase.estado !== Estados.DevAnalista){
            this.f.libreriaFuente.disable();
            this.f.libreriaObjeto.disable();
            this.f.libreriaObjetoPFLF.disable();
            this.f.motivoPase.disable();
          }

          break;
      }
 
  }

  private configValidations(){
    switch(this.tipoPase){
      case TipoPlataforma.AplicacionWeb:
      case TipoPlataforma.PaginasWeb:
      case TipoPlataforma.Spring:
        this.f?.fuente.setValidators([Validators.required]);
        break;
      case TipoPlataforma.AS400_IBS:
        // this.f?.libreriaFuente.setValidators([Validators.required]);
        // this.f?.libreriaObjeto.setValidators([Validators.required]);
        // this.f?.libreriaObjetoPFLF.setValidators([Validators.required]);
        break;
    }
  }

  private cargarFuentesyObjetos(){

    if(TipoPlataforma.AS400_IBS === this.tipoPase){

      forkJoin([
        this.registroPaseService.ObtenerListaLibrerias(1),
        this.registroPaseService.ObtenerListaLibrerias(2),
        this.registroPaseService.ObtenerListaLibrerias(3)
      ]).subscribe(data => {
        this.lFuentes = data[0].response as Libreria[];
        this.lObjetos = data[1].response as Libreria[];
        this.lObjetosPFyLF = data[2].response as Libreria[];
      },
      err => {
        console.log(err);
      })

    }
  }
  
  get f(){
    return this.datosGenerales?.controls;
  }

  listarAnalistas():void{
    this.registroPaseService.ObtenerListaAnalistas().subscribe((result: any)=>{
      this.lAnalistas = result.response as Analista[];
    }, (error)=> {
      console.log(error);
    });
  }

}