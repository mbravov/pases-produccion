import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[simpleSpaces]'
})
export class SimpleSpacesDirective {

  constructor(private el: ElementRef) {
  
  }

  @HostListener('keypress', ['$event']) onKeyPress(event: any) {
    let current: string = this.el.nativeElement.value;
    let next: string = current.concat(event.key);
    if (current === "" && next === " ") {
      event.preventDefault();
      return;
    }
    if (current === " ") {
      event.preventDefault();
      return;
    } else if (current.length > 1) {
      if (current.slice(current.length - 1) === " " && event.key === " ") {
        event.preventDefault();
        return;
      } 
    }
  }

}
