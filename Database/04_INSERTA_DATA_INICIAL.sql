USE DBAgrSGPP
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 100 OR Grupo = 100)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 100 OR Grupo = 100
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado) 
VALUES 
(0, 100, 'Tipo Pase', NULL, NULL, 0, 1),
(100, 1, 'Aplicaciones Web', NULL, NULL, 1, 1),
(100, 2, 'AS/400 � IBS', NULL, NULL, 2, 1),
(100, 3, 'P�ginas Web',NULL, NULL, 3, 1),
(100, 4, 'Spring',NULL, NULL, 4, 1);
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 200 OR Grupo = 200)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 200 OR Grupo = 200
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado) 
VALUES 
(0, 200, 'Estados Pase', NULL, NULL, 0, 1),
(200, 1, 'DerEjecutivo', 'Derivado ejecutivo de sistemas','DES', 1, 1),
(200, 2,  'DerCalidad', 'Derivado a funcionario de calidad', 'DFC', 2, 1),
(200, 3,  'DerSoporte', 'Derivado a funcionario de soporte','DFS', 3, 1),
(200, 4,  'DevAnalista','Devuelto analista', 'DVA', 4, 1),
(200, 5,  'Aprobado','Aprobado', 'APR', 5, 1),
(200, 6,  'Anulado','Anulado', 'ANU', 6, 1);
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 300 OR Grupo = 300)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 300 OR Grupo = 300
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado) 
VALUES 
(0, 300, 'Motivo Pase', NULL, NULL, 0, 1),
(300, 1, 'Desarrollo', NULL, NULL, 1, 1),
(300, 2, 'Mantenimiento', NULL, NULL, 2, 1),
(300, 3, 'Incidencia',NULL, NULL, 3, 1)
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 400 OR Grupo = 400)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 400 OR Grupo = 400
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado) 
VALUES 
(0, 400, 'Tipo Objeto', NULL, NULL, 0, 1),
(400, 1, 'QDDSSRC', NULL, NULL, 1, 1),
(400, 2, 'QCLSRC', NULL, NULL, 2, 1),
(400, 3, 'QRPGSRC',NULL, NULL, 3, 1)
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 500 OR Grupo = 500)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 500 OR Grupo = 500
END
GO

INSERT INTO dbo.Parametro (Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado)
VALUES (0, 500, 'Informacion Soporte', NULL, NULL, 0, 1)
GO

INSERT INTO dbo.Parametro (Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado)
VALUES (500, 1, 'Titulo', 'Para Proyectos/Mejoras', '1', 1, 1),
	   (500, 1, 'Descripcion', 'Requerimientos de Usuario.', '1', 2, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_EspecificacionesFuncionales', '1', 2, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_Pase_Produccion', '1', 2, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_Instalaci�n_Configuraciones', '1', 2, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_CasosPrueba', '1', 2, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_EvidenciaPruebasDesarrollo', '1', 2, 1),
	   (500, 1, 'Titulo', 'Para Incidencias:', '2', 1, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_Pase_Produccion', '2', 2, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_Instalaci�n_Configuraciones', '2', 2, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_Informe_Incidencia', '2', 2, 1),
	   (500, 1, 'Descripcion', 'Ticket de Incidencia', '2', 2, 1),
	   (500, 1, 'Descripcion', 'AGR_Sistemas_EvidenciaPruebasDesarrollo', '2', 2, 1),
	   (500, 1, 'Titulo', 'Ruta de Documentaci�n:', '3', 1, 1),
	   (500, 1, 'Descripcion', 'G:\Procesos y Tecnologia\Sistemas\TI\0.Formatos_Pase_Producci�n', '3', 2, 1),
	   (500, 2, 'Titulo', 'Para Proyectos/Mejoras', '1', 1, 1),
	   (500, 2, 'Descripcion', 'Requerimientos de Usuario.', '1', 2, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_EspecificacionesFuncionales', '1', 2, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_Pase_Produccion', '1', 2, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_Instalaci�n_Configuraciones', '1', 2, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_CasosPrueba', '1', 2, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_EvidenciaPruebasDesarrollo', '1', 2, 1),
	   (500, 2, 'Titulo', 'Para Incidencias:', '2', 1, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_Pase_Produccion', '2', 2, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_Instalaci�n_Configuraciones', '2', 2, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_Informe_Incidencia', '2', 2, 1),
	   (500, 2, 'Descripcion', 'Ticket de Incidencia', '2', 2, 1),
	   (500, 2, 'Descripcion', 'AGR_Sistemas_EvidenciaPruebasDesarrollo', '2', 2, 1),
	   (500, 2, 'Titulo', 'Ruta de Documentaci�n:', '3', 1, 1),
	   (500, 2, 'Descripcion', 'G:\Procesos y Tecnologia\Sistemas\TI\0.Formatos_Pase_Producci�n', '3', 2, 1),
	   (500, 3, 'Titulo', 'Para pruebas y/o sustento debera adjuntar firmado:', '2', 1, 1),
	   (500, 3, 'Descripcion', 'Hoja del Requerimiento del Usuario.', '2', 2, 1),
	   (500, 3, 'Descripcion', 'Acta de Conformidad.', '2', 2, 1),
	   (500, 3, 'Descripcion', 'Formato de Pase a Producci�n.', '2', 2, 1),
	   (500, 4, 'Titulo', 'Para pruebas y/o sustento debera adjuntar firmado:', '2', 1, 1),
	   (500, 4, 'Descripcion', 'Hoja del Requerimiento del Usuario.', '2', 2, 1),
	   (500, 4, 'Descripcion', 'Acta de Conformidad.', '2', 2, 1),
	   (500, 4, 'Descripcion', 'Formato de Pase a Producci�n.', '2', 2, 1)
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 600 OR Grupo = 600)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 600 OR Grupo = 600
END
GO

INSERT INTO dbo.Parametro (Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado)
VALUES (0, 600, 'Compilacion', NULL, NULL, 0, 1)
GO

INSERT INTO dbo.Parametro (Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado)
VALUES (600, 1, 'Inmediata', NULL, NULL, 1, 1),
	   (600, 2, 'Condiciones Especiales', NULL, NULL, 2, 1)
GO

INSERT INTO dbo.Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, UsuarioModificacion, FechaRegistro, FechaModificacion)
VALUES ('EJS', 'Ejecutivo de Sistemas', 'fnima@agrobanco.com.pe', 'AGRFNF', 'Freddy Nima Flores', 1, 'USER', NULL, GETDATE(), NULL),
	   ('EJS', 'Ejecutivo de Sistemas', 'cargote@agrobanco.com.pe', 'AGRCACU', 'Cesar Argote Cueva', 1, 'USER', NULL, GETDATE(), NULL),
	   ('CAL', 'Funcionario de Calidad', 'loc_jtalledo@agrobanco.com.pe', 'AGRJTA', 'Jorge Talledo', 1, 'USER', NULL, GETDATE(), NULL),
	   ('SOP', 'Funcionario de Soporte', 'proman@agrobanco.com.pe', 'AGRPRI', 'Pedro Roman Injante', 1, 'USER', NULL, GETDATE(), NULL),
	   ('SOP', 'Funcionario de Soporte', 'jgandolfo@agrobanco.com.pe', 'AGRJJGS', 'Jos� Gandolfo Salazar', 1, 'USER', NULL, GETDATE(), NULL),
	   ('ANA', 'Analista', 'wllamocca@agrobanco.com.pe', 'AGRWLF', 'William Llamocca Fernandez', 1, 'USER', NULL, GETDATE(), NULL),
	   ('ANA', 'Analista', 'wlinares@agrobanco.com.pe', 'AGRWLH', 'Wiliam Linares Huaringa', 1, 'USER', NULL, GETDATE(), NULL),
	   ('ANA', 'Analista', 'fnima@agrobanco.com.pe', 'AGRFNF', 'Freddy Nima Flores', 1, 'USER', NULL, GETDATE(), NULL),
	   ('ANA', 'Analista', 'loc_evasquez@agrobanco.com.pe', 'AGREVP', 'Elizabeth Vasquez ', 1, 'USER', NULL, GETDATE(), NULL),
	   ('ANA', 'Analista', 'avega@agrobanco.com.pe', 'AGRAVM', 'Arturo Vega', 1, 'USER', NULL, GETDATE(), NULL)

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 700 OR Grupo = 700)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 700 OR Grupo = 700
END
GO

INSERT INTO dbo.Parametro (Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado)
VALUES (0, 700, 'Librer�a fuentes', NULL, NULL, 0, 1),
	   (700, 1, 'AGRCYFILES', NULL, NULL, 1, 1),
	   (700, 2, 'AGRFIFILES', NULL, NULL, 2, 1),
	   (700, 3, 'AGRRCCLIB', NULL, NULL, 3, 1),
	   (700, 4, 'AGRUSRLIB', NULL, NULL, 4, 1),
       (700, 5, 'LIBRFIN', NULL, NULL, 5, 1)
GO   

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 800 OR Grupo = 800)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 800 OR Grupo = 800
END
GO

INSERT INTO dbo.Parametro (Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado)
VALUES (0, 800, 'Librer�a objetos', NULL, NULL, 0, 1),
	   (800, 1, 'AGRCYFILES', NULL, NULL, 1, 1),
	   (800, 2, 'AGRFIFILES', NULL, NULL, 2, 1),
	   (800, 3, 'AGRRCCLIB', NULL, NULL, 3, 1),
	   (800, 4, 'AGRSBSLIB', NULL, NULL, 4, 1),
	   (800, 5, 'AGRUSRLIB', NULL, NULL, 5, 1),
	   (800, 6, 'LIBRFIN', NULL, NULL, 6, 1)
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 900 OR Grupo = 900)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 900 OR Grupo = 900
END
GO

INSERT INTO dbo.Parametro (Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado)
VALUES (0, 900, 'Librer�a objetos (PY y LF)', NULL, NULL, 0, 1),
	   (900, 1, 'AGRCYFILES', NULL, NULL, 1, 1),
	   (900, 2, 'AGRFIFILES', NULL, NULL, 2, 1),
	   (900, 3, 'AGRRCCLIB', NULL, NULL, 3, 1),
	   (900, 4, 'AGRSBSLIB', NULL, NULL, 4, 1),
	   (900, 5, 'AGRUSRLIB', NULL, NULL, 5, 1),
	   (900, 6, 'LIBRFIN', NULL, NULL, 6, 1)
GO	   

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1000 OR Grupo = 1000)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1000 OR Grupo = 1000
END
GO

INSERT INTO dbo.Parametro (Grupo, Parametro, Descripcion, Valor, Referencia, Orden, Estado)
VALUES (0, 1000, 'Modulo (PY y LF)', NULL, NULL, 0, 1),
	   (1000, 1, 'Activo fijo', NULL, NULL, 1, 1),
	   (1000, 2, 'Contabilidad', NULL, NULL, 2, 1),
	   (1000, 3, 'Cuentas por pagar', NULL, NULL, 3, 1),
	   (1000, 4, 'Licitaciones', NULL, NULL, 4, 1),
	   (1000, 5, 'Log�stica', NULL, NULL, 5, 1),
	   (1000, 6, 'Planilla', NULL, NULL, 6, 1),
	   (1000, 7, 'Presupuesto', NULL, NULL, 7, 1)
GO	   